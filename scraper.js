const got = require("got");
const cheerio = require("cheerio");
const url =
  "https://www.finn.no/bap/forsale/search.html?category=0.93&product_category=2.93.3217.39&q=iPhone+7&search_type=SEARCH_ID_BAP_ALL&sub_category=1.93.3217";

const scrape = async url => {
  try {
    const response = await got(url);
    const $ = cheerio.load(response.body);

    const data = [];
    $(".result-item").each((i, e) => {
      //Scrape the titles, URL and price of every element in a Finn.no search
      const title = $(e)
        .find("h3")
        .text();
      const url = $(e)
        .find("a[data-finnkode]")
        .attr("href");
      const price = parseInt(
        $(e)
          .find("p.result-price")
          .text()
          .replace(/\s/g, "") //remove whitespace
      );

      data.push({
        title: title,
        url: url,
        price: price
      });
    });

    console.log(data);
    checkAveragePrice(data);
    return data;
  } catch (error) {
    console.log(error);
  }
};

const checkAveragePrice = array => {
  var total = 0;
  for (var i = 0; i < array.length; i++) {
    if (!array[i]["price"] === undefined) {
      total += array[i]["price"];
    }
  }
  console.log("Total: " + total);
  return total;
};

scrape(url);
